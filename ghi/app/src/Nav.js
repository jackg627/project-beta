import { Link } from "react-router-dom";
import { useState } from "react";

function Nav() {
  const [openSales, setOpenSales] = useState();
  const [openInventory, setOpenInventory] = useState();

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success" id="navbar">
      <div className="container-fluid">
        <Link className="navbar-brand" to="/">
          CarCzar Home
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="collapse navbar-collapse"
          id="navbarSupportedContent"
        >
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="dropdown">
              <button
                id="navbar-dropdown"
                className="btn btn-success dropdown-toggle"
                onClick={() => setOpenInventory(openInventory)}
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <Link className="dropdown-item" to="/manufacturers/">
                    View Manufacturers
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/manufacturers/new/">
                    Add New Manufacturer
                  </Link>
                </li>
                <div className="dropdown-divider"></div>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/models/">
                    View Models
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/models/new/">
                    Add New Vehicle Model
                  </Link>
                </li>
                <div className="dropdown-divider"></div>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/automobiles/">
                    View Automobiles
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/automobiles/new/">
                    Add New Automobile
                  </Link>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button
                id="navbar-dropdown"
                className="btn btn-success dropdown-toggle"
                onClick={() => setOpenSales(openSales)}
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <Link className="dropdown-item" to="/customers/">
                    View Customers
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/customers/new/">
                    New Customer Form
                  </Link>
                  <div className="dropdown-divider"></div>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/salespersons/">
                    View Sales Staff
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/salespersons/new/">
                    New Salesperson Form
                  </Link>
                </li>
                <div className="dropdown-divider"></div>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/salesrecords/">
                    View All Sales
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/salesrecords/new/">
                    New Sale Form
                  </Link>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button
                id="navbar-dropdown"
                className="btn btn-success dropdown-toggle"
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <Link className="dropdown-item" to="/technicians/">
                    View Technicians
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/technicians/new/">
                    New Technician Form
                  </Link>
                </li>
                <div className="dropdown-divider"></div>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/appointments/">
                    View Active Appointments
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/appointments/new">
                    New Appointment Form
                  </Link>
                </li>
                <div className="dropdown-divider"></div>
                <li className="nav-item">
                  <Link
                    className="dropdown-item"
                    to="/appointments/search/"
                  >
                    Search Service History
                  </Link>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
