import React from 'react';
import './index.css'

function MainPage() {
  return (
<>
<div className="d-flex flex-column">
<div className="my-5 container">
      <div className="card text-center shadow d-flex flex-column" id="main-style" >
        <div className="card-body">
          {/* <div className="container py-5"> */}
            <div className="row justify-content-center">
              <div>
                <h3 id="welcome-text">Welcome to</h3>
                <p className="display-2 text-center fw-bold" id="site-title">CarCzar!</p>
              </div>
              <div className="col-lg-8" style={{margin:'10px'}}>
                Your premiere solution for automobile dealership management!
              </div>
              <div>
                <a href="automobiles/" className="btn btn-primary">Inventory</a>
                <a href="salesrecords/" className="btn btn-primary">Sales</a>
                <a href="appointments/" className="btn btn-primary">Service</a>
              </div>
            </div>
          {/* </div> */}
        </div>
      </div>
    </div>

</div>
</>

  );
}

export default MainPage;
