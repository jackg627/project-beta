import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom"

const VehicleModelForm = () => {
  const [name, setName] = useState("");
  const [picture_url, setPicture] = useState("");
  const [manufacturer_id, setManufacturer] = useState("");
  const [manufacturer, setManufacturers] = useState([]);
  const [models, setModel] = useState([]);


  const getModelData = async () => {
    const resp = await fetch("http://localhost:8100/api/models/");
    if (resp.ok) {
      const data = await resp.json();
      setModel(data.models);
    }
  };

  const getManufacturerData = async () => {
    const resp = await fetch("http://localhost:8100/api/manufacturers/");
    if (resp.ok) {
      const data = await resp.json();
      const sortedManufacturers = data.manufacturers.sort((a, b) =>
      a.name.localeCompare(b.name)
      );
      setManufacturers(sortedManufacturers);
      console.log(data)
    }
  };


  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.picture_url = picture_url;
    data.manufacturer_id = manufacturer_id;
    data.manufacturer = manufacturer;
    const int_mfg_id = parseInt(data.manufacturer_id)
    for (let item of models) {
      if (int_mfg_id === item.manufacturer.id && data.name === item.name) {
        alert("That model has already been created!")
        setName("");
        setPicture("");
        setManufacturer("");
        return
      }
    }
    const modelUrl = "http://localhost:8100/api/models/";
    const fetchConfigs = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const resp = await fetch(modelUrl, fetchConfigs);
    if (resp.ok) {
      setName("");
      setPicture("");
      setManufacturer("");
      alert("Created new Vehicle Model!")
    } else {
      alert("Failed to create new Vehicle Model")
    }
  };



  useEffect(() => {
    getManufacturerData();
    getModelData();
  }, []);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
  };

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };



  return (
    <div className="table-container">
      <div className = "link-button-div-form">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/models"
          id="link-button">
            View Models
          </Link>
        </button>
      </div>
      <div className="my-5">
        <div className="offset-3 col-10" id="form-style">
          <div className="shadow p-4 mt-4">
            <h1>Add a new model</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <select
                  value={manufacturer_id}
                  onChange={handleManufacturerChange}
                  required
                  id="manufacturer_id"
                  className="form-control"
                  name="manufacturer_id"
                >
                  <option value="">Choose a manufacturer</option>
                  {manufacturer.map((manufacturer) => {
                    return (
                      <option
                        key={manufacturer.href}
                        value={manufacturer.id}
                      >
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={name}
                  onChange={handleNameChange}
                  placeholder="name"
                  required
                  type="text"
                  id="name"
                  className="form-control"
                  name="name"
                />
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={picture_url}
                  onChange={handlePictureChange}
                  placeholder="picture_url"
                  required
                  type="text"
                  id="picture_url"
                  className="form-control"
                  name="picture_url"
                />
                <label htmlFor="name">Picture URL</label>
              </div>
              <button className="btn btn-primary">Create Model</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VehicleModelForm;
