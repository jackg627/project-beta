import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom"

const AutomobileForm = () => {
  const [models, setModels] = useState([]);
  const [inventory, setInventoryData] = useState([]);

  const [automobileData, setAutomobileData] = useState({
    vin: "",
    model: "",
    color: "",
    year: "",
  });

  const getInventoryData = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      setInventoryData(data.autos);
    }
  };

  const getModelsData = async () => {
    const resp = await fetch("http://localhost:8100/api/models/");
    if (resp.ok) {
      const data = await resp.json();
      const sortedModels = data.models.sort((a, b) =>
      a.name.localeCompare(b.name)
      );
      setModels(sortedModels);
    }
  };

  useEffect(() => {
    getModelsData();
    getInventoryData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    for (let item of inventory) {
      if (automobileData.vin === item.vin) {
        alert("That vehicle already exists in inventory!")
        setAutomobileData({
          vin: "",
          model: "",
          color: "",
          year: "",
          model_id: ""
        })
        return;
      }
    }
    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(automobileData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      setAutomobileData({
        vin: "",
        model: "",
        color: "",
        year: "",
        model_id: ""
      });
      alert("Created new automobile!");
    } else {
      alert("Failed to create new automobile");
    }
  };

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setAutomobileData({
      ...automobileData,
      [inputName]: value,
    });
  };
  return (
    <div className="table-container">
      <div className = "link-button-div-form">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/automobiles"
          id="link-button">
            View Automobiles
          </Link>
        </button>
      </div>
      <div className="my-5">
        <div className="offset-3 col-10 " id="form-style">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Automobile</h1>
            <form onSubmit={handleSubmit} id="create-automobile-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleAutomobileChange}
                  value={automobileData.vin}
                  placeholder="VIN"
                  required
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleAutomobileChange}
                  value={automobileData.model_id}
                  required
                  name="model_id"
                  id="model"
                  className="form-select"
                >
                  <option value="">Choose a Model</option>
                  {models.map((model) => {
                    return (
                      <option key={model.id} value={model.id}>
                        {model.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleAutomobileChange}
                  value={automobileData.color}
                  placeholder="Color"
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleAutomobileChange}
                  value={automobileData.year}
                  placeholder="Year"
                  required
                  type="text"
                  name="year"
                  id="year"
                  className="form-control"
                />
                <label htmlFor="year">Year</label>
              </div>

              <button className="btn btn-primary">Create Automobile</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AutomobileForm;
