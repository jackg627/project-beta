import { useState, useEffect } from "react";
import { Link } from "react-router-dom"
import '../../index.css'

const ManufacturerList = () => {
  const [manufacturers, setManufacturer] = useState([]);
  const [filterTerm, setFilterTerm] = useState("");

  const getManufacturerData = async () => {
    const resp = await fetch("http://localhost:8100/api/manufacturers/");
    if (resp.ok) {
      const data = await resp.json();
      const sortedManufacturers = data.manufacturers.sort((a, b) =>
      a.name.localeCompare(b.name)
      );
      setManufacturer(sortedManufacturers);
    }
  };

  useEffect(() => {
    getManufacturerData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  const handleDelete = async (e) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this Manufacturer? This cannot be undone!");
    if (!confirmDelete) {
      return;
    }
    const url = `http://localhost:8100/api/manufacturers/${e.target.id}`;

    const fetchConfigs = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resp = await fetch(url, fetchConfigs);
    const data = await resp.json();

    setManufacturer(
      manufacturers.filter(
        (manufacturer) => String(manufacturer.id) !== e.target.id
      )
    );
  };

  return (
    <div className="table-container">
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/manufacturers/new"
          id="link-button">
            Add New Manufacturer
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "43px", paddingRight: "43px"}}
        >
          <Link
          to="/models"
          id="link-button">
            View Models
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "24px", paddingRight: "24px"}}
        >
          <Link
          to="/automobiles"
          id="link-button">
            View Automobiles
          </Link>
        </button>
      </div>
      <table className="table table-striped">
        <thead>
          <div className="table-name">Manufacturers</div>
          <div className="form-outline">
            <input
              onChange={handleFilterChange}
              type="search"
              id="form1"
              className="form-control"
              placeholder="To search for a specific Manufacturer, enter name here"
              aria-label="Search"
            />
          </div>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.filter((manufacturer) =>
          manufacturer.name.toLowerCase().includes(filterTerm)
          )
          .map((manufacturer) => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
                <td>
                  <button
                    onClick={handleDelete}
                    id={manufacturer.id}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default ManufacturerList;
