import { useState, useEffect } from "react";
import { Link } from "react-router-dom"

const VehicleModelList = () => {
  const [models, setModels] = useState([]);
  const [filterTerm, setFilterTerm] = useState("");

  const getModelsData = async () => {
    const resp = await fetch("http://localhost:8100/api/models/");
    if (resp.ok) {
      const data = await resp.json();
      const sortedModels = data.models.sort((a, b) =>
      a.name.localeCompare(b.name)
      );
      setModels(sortedModels);
    }
  };

  useEffect(() => {
    getModelsData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  const handleDelete = async (e) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this Model? This cannot be undone!");
    if (!confirmDelete) {
      return;
    }
    const url = `http://localhost:8100/api/models/${e.target.id}`;
    const fetchConfigs = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const resp = await fetch(url, fetchConfigs);
    const data = await resp.json();
    setModels(models.filter((model) => String(model.id) !== e.target.id));
  };

  return (
    <div className="table-container">
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "18px", paddingRight: "18px"}}
        >
          <Link
          to="/models/new"
          id="link-button">
            Add New Model
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/manufacturers"
          id="link-button">
            View Manufacturers
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "11.5px", paddingRight: "11.5px"}}
        >
          <Link
          to="/automobiles"
          id="link-button">
            View Automobiles
          </Link>
        </button>
      </div>
      <table className="table table-striped">
        <thead>
        <div className="table-name">Models</div>
          <div className="form-outline">
            <input
              onChange={handleFilterChange}
              type="search"
              id="form1"
              className="form-control"
              placeholder="To search for a specific Model, enter name here"
              aria-label="Search"
              />
            </div>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>
          {models.filter((model) =>
          model.name.toLowerCase().includes(filterTerm)
          )
          .map((model) => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img
                    src={model.picture_url}
                    width="100"
                    alt="carPicture"
                  />
                </td>
                <td>
                  <button
                    onClick={handleDelete}
                    id={model.id}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default VehicleModelList;
