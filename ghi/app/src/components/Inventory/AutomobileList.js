import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom"
import '../../index.css'

const AutomobileList = () => {
  const [automobile, setAutomobileData] = useState([]);
  const [filterTerm, setFilterTerm] = useState("");

  const getAutomobileData = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      const sortedAutomobiles = data.autos.sort((a, b) =>
      a.vin.localeCompare(b.vin)
      );
      setAutomobileData(data.autos);
    }
  };

  const handleDelete = async (vin) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this Automobile? This cannot be undone!");
    if (!confirmDelete) {
      return;
    }
    const automobileUrl = `http://localhost:8100/api/automobiles/${vin}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      getAutomobileData();
    }
  };

  useEffect(() => {
    getAutomobileData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  return (
    <div className="table-container">
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/automobiles/new"
          id="link-button">
            Add New Automobile
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "36px", paddingRight: "36px"}}
        >
          <Link
          to="/models"
          id="link-button">
            View Models
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "11px", paddingRight: "11px"}}
        >
          <Link
          to="/manufacturers"
          id="link-button">
            View Manufacturers
          </Link>
        </button>
      </div>
      <table className="table table-striped" style={{color: '#ffffff'}}>
        <thead>
        <div className="table-name">Automobile Inventory</div>
          <div className="form-outline">
            <input
              onChange={handleFilterChange}
              type="search"
              id="form1"
              className="form-control"
              placeholder="To search for a specific Automobile, enter VIN here"
              aria-label="Search"
              />
          </div>
          <tr>
            <th>VIN</th>
            <th>Year</th>
            <th>Color</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {automobile &&
            automobile.filter((autos) =>
            autos.vin.toLowerCase().includes(filterTerm)
            )
            .map((autos) => {
              return (
                <tr key={autos.id}>
                  <td>{autos.vin}</td>
                  <td>{autos.year}</td>
                  <td>{autos.color}</td>
                  <td>{autos.model.manufacturer.name}</td>
                  <td>{autos.model.name}</td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => handleDelete(autos.vin)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
};

export default AutomobileList;
