import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom"

const ManufacturerForm = () => {
  const [name, setName] = useState("");
  const [manufacturers, setManufacturer] = useState([])

  const getManufacturerData = async () => {
    const resp = await fetch("http://localhost:8100/api/manufacturers/");
    if (resp.ok) {
      const data = await resp.json();
      setManufacturer(data.manufacturers);
    }
    };

    useEffect(() => {
      getManufacturerData();
    }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    for (let item of manufacturers) {
      if (data.name === item.name) {
        alert("That manufacturer has already been created!")
        setName("")
        return
      }
    }
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfigs = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resp = await fetch(manufacturerUrl, fetchConfigs);
    if (resp.ok) {
      setName("");
      alert("Created new manufacturer!")
    } else {
      alert("Failed to create new manufacturer")
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  return (
    <div className="table-container">
      <div className="link-button-div-form">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/manufacturers"
          id="link-button">
            View Manufacturers
          </Link>
        </button>
      </div>
      <div className="my-5">
        <div className="offset-3 col-10" id="form-style">
          <div className="shadow p-4 mt-4">
            <h1>Add a new manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input
                  value={name}
                  onChange={handleNameChange}
                  placeholder="Name"
                  required
                  type="text"
                  id="name"
                  className="form-control"
                  name="name"
                />
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-primary">Create Manufacturer</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ManufacturerForm;
