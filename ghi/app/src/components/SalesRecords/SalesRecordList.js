import { useEffect, useState } from "react";
import { Link } from "react-router-dom"

const SalesRecordList = () => {
  const [sales_records, setSalesRecordData] = useState([]);
  const [filterValue, setFilterValue] = useState("");
  const [filterStaff, setFilterStaff] = useState("");

  const getSalesRecordData = async () => {
    const response = await fetch("http://localhost:8090/api/salesrecords/");
    if (response.ok) {
      const data = await response.json();
      setSalesRecordData(data.sales_records);
    }
  };

  useEffect(() => {
    getSalesRecordData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterValue(e.target.value.toLowerCase());
  };

  return (
<div className="table-container">
  <div className = "link-button-div-list">
    <button
      className="btn btn-primary"
      id="link-button-spacing"
    >
      <Link
      to="/salesrecords/new"
      id="link-button">
        Create New Sale
      </Link>
    </button>
  </div>
  <div className = "link-button-div-list">
    <button
      className="btn btn-primary"
      id="link-button-spacing"
      style={{paddingLeft: "8px", paddingRight: "8px"}}
    >
      <Link
      to="/salespersons"
      id="link-button">
        View Sales Staff
      </Link>
    </button>
  </div>
  <div className = "link-button-div-list">
    <button
      className="btn btn-primary"
      id="link-button-spacing"
      style={{paddingLeft: "6px", paddingRight: "6px"}}
    >
      <Link
      to="/customers"
      id="link-button">
        View Customers
      </Link>
    </button>
  </div>
  <table className="table table-striped">
    <thead>
    <div className="table-name">Sales Records</div>
    <div className="form-outline" >
      <input
        onChange={handleFilterChange}
        type="search"
        id="form1"
        className="form-control"
        placeholder="Type to search by employee name"
        aria-label="Search"

      />
    </div>
      <tr>
        <th>Salesperon</th>
        <th>Employee ID</th>
        <th>Price</th>
        <th>Automobile</th>
        <th>Customer</th>
      </tr>
    </thead>
    <tbody>
      {sales_records &&
        sales_records
        .filter((sale) =>
        sale.salesperson.salesperson_name.toLowerCase().includes(filterValue)
      )
      .filter((sale) =>
        sale.salesperson.salesperson_name
          .toLowerCase()
          .includes(filterStaff.toLowerCase())
      ).map((sale) => {
          return (
            <tr key={sale.id}>
              <td>{sale.salesperson.salesperson_name}</td>
              <td>{sale.salesperson.id}</td>
              <td>{sale.price}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.customer.customer_name}</td>
            </tr>
          );
        })}
    </tbody>
  </table>
</div>
  );
};

export default SalesRecordList;
