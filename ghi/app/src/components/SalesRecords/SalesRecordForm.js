import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom"

const SalesRecordForm = () => {
  const [automobiles, setAutomobileData] = useState([]);
  const [salesperson, setSalespersonData] = useState([]);
  const [customers, setCustomerData] = useState([]);
  const [price, setPriceData] = useState("");
  const [selectedAutomobile, setSelectedAutomobile] = useState("");
  const [selectedSalesperson, setSelectedSalesperson] = useState("");
  const [selectedCustomer, setSelectedCustomer] = useState("");
  const [salesRecords, setSalesRecordData] = useState([]);


  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setSelectedCustomer(value);
  };

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSelectedSalesperson(value);
  };

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setSelectedAutomobile(value);
  };

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPriceData(value);
  };

  const getCustomerData = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      const sortedCustomers = data.customers.sort((a, b) =>
      a.customer_name.localeCompare(b.customer_name))
      setCustomerData(sortedCustomers);
    }
  };

  const getSalespersonData = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      const sortedSalespeople = data.salesPeople.sort((a, b) =>
      a.salesperson_name.localeCompare(b.salesperson_name))
      setSalespersonData(sortedSalespeople);
    }
  };

  const getAutomobileData = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      const sortedAutomobiles = data.autos.sort((a, b) =>
      a.vin.localeCompare(b.vin)
      );
      setAutomobileData(data.autos);
    }
  };

  const getSalesRecord = async () => {
    const salesrecordUrl = "http://localhost:8090/api/salesrecords/";
    const response = await fetch(salesrecordUrl);
    if (response.ok) {
      const data = await response.json();
      setSalesRecordData(data.sales_records);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.automobiles = selectedAutomobile;
    data.salesperson = selectedSalesperson;
    data.customers = selectedCustomer;
    data.price = price;
    for (let item of salesRecords) {
      if (data.automobiles === item.automobile.vin) {
        const confirmSale = window.confirm("This vehicle has already been sold by us before! Proceed?")
        if (!confirmSale) {
          setSelectedAutomobile("");
          setSelectedSalesperson("");
          setSelectedCustomer("");
          setPriceData("");
          event.target.reset();
          return;
        }
      }
    }
    const automobileUrl = `http://localhost:8100/api/automobiles/${selectedAutomobile}`;
    const salesrecordUrl = "http://localhost:8090/api/salesrecords/";
    const fetchConfig1 = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const fetchConfig2 = {
      method: "Delete",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      }
    }
    const response1 = await fetch(salesrecordUrl, fetchConfig1);
    const response2 = await fetch(automobileUrl, fetchConfig2)
    if (response1.ok && response2.ok) {
      setSelectedAutomobile("");
      setSelectedSalesperson("");
      setSelectedCustomer("");
      setPriceData("");
      event.target.reset();
      alert("Created new sale record!");
    } else {
      alert("Failed to create new sale record");
    }
  };

  useEffect(() => {
    getCustomerData();
    getSalespersonData();
    getAutomobileData();
    getSalesRecord();
  }, []);

  return (
<div className="table-container">
  <div className = "link-button-div-form">
    <button
      className="btn btn-primary"
      id="link-button-spacing"
    >
      <Link
      to="/salesrecords"
      id="link-button">
        View All Sales
      </Link>
    </button>
  </div>
  <div className="my-5">
    <div className="offset-3 col-6" id="form-style">
      <div className="shadow p-4 mt-4">
        <h1>Post New Sale</h1>
        <form id="create-sale-form" onSubmit={handleSubmit}>
          <div className="mb-3">
            <select
              required
              name="automobile"
              id="automobile"
              className="form-select"
              onChange={handleAutomobileChange}
            >
              <option value="">Choose Automobile</option>
              {automobiles.map((auto) => (
                <option key={auto.vin} value={auto.vin}>
                  {auto.vin}
                </option>
              ))}
            </select>
          </div>
          <div className="mb-3">
            <select
              required
              name="sales_person"
              id="sales_person"
              className="form-select"
              onChange={handleSalespersonChange}
            >
              <option value="">Choose Salesperson</option>
              {salesperson &&
                salesperson.map((person) => (
                  <option key={person.employee_number} value={person.employee_number}>
                    {person.salesperson_name}
                  </option>
                ))}
            </select>
          </div>
          <div className="mb-3">
            <select
              required
              name="customer_name"
              id="customer_name"
              className="form-select"
              onChange={handleCustomerChange}
            >
              <option value="">Choose Customer</option>
              {customers &&
                customers.map((customer) => (
                  <option key={customer.id} value={customer.id}>
                    {customer.customer_name}
                  </option>
                ))}
            </select>
          </div>
          <div className="form-floating mb-3">
            <input
              placeholder="price"
              onChange={handlePriceChange}
              required
              type="text"
              name="price"
              id="price"
              className="form-control"
            />
            <label htmlFor="price">Price</label>
          </div>
          <button className="btn btn-primary">Complete Sale</button>
        </form>
      </div>
    </div>
  </div>
</div>
  );
};

export default SalesRecordForm;
