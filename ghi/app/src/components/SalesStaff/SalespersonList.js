import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom"

const SalespersonList = () => {
  const [salespersonData, setSalespersonData] = useState();
  const [filterTerm, setFilterTerm] = useState("");

  const getSalespersonData = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      const sortedSalespeople = data.salesPeople.sort((a, b) =>
      a.salesperson_name.localeCompare(b.salesperson_name))
      setSalespersonData(sortedSalespeople);
    }
  };

  useEffect(() => {
    getSalespersonData();
  },[]);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  const handleDelete = async (e) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this Salesperson? This cannot be undone!");
    if (!confirmDelete) {
      return;
    }

    const salespeopleUrl = `http://localhost:8090/api/salespeople/${e.target.id}/`;
    const fetchConfig = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salespeopleUrl, fetchConfig);
    if (response.ok) {
      setSalespersonData();
    } else {
      alert("Salesperson was not found!");
    }
  };


  return (
    <div className="table-container">
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/salespersons/new"
          id="link-button">
            Add New Salesperson
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "15px", paddingRight: "15px"}}
        >
          <Link
          to="/salesrecords"
          id="link-button">
            View Sales Records
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "25px", paddingRight: "25px"}}
        >
          <Link
          to="/customers"
          id="link-button">
            View Customers
          </Link>
        </button>
      </div>
      <table className="table table-striped">
        <thead>
          <div className="table-name">Sales Staff</div>
          <div className="form-outline">
          <input
            onChange={handleFilterChange}
            type="search"
            id="form1"
            className="form-control"
            placeholder="To search for a specific Salesperson, enter name here"
            aria-label="Search"
          />
        </div>
          <tr>
            <th>Salesperson Name</th>
            <th>Salesperson ID</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {salespersonData &&
          salespersonData.filter((person) =>
            person.salesperson_name.toLowerCase().includes(filterTerm)
            )
            .map((person) => {
              return (
                <tr key={person.id}>
                  <td>{person.salesperson_name}</td>
                  <td>{person.id}</td>

                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={handleDelete}
                      id={person.id}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
};

export default SalespersonList;
