import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom"

const CustomerForm = () => {
  const [customer_name, setCustomerNameData] = useState("");
  const [address, setCustomerAddress] = useState("");
  const [phone_number, setCustomerPhoneNumber] = useState("");
  const [customerData, setCustomerData] = useState([]);

  const getCustomerData = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      setCustomerData(data.customers);
    }
  };

  useEffect(() => {
    getCustomerData();
  }, []);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setCustomerNameData(value);
  };

  const handleAddressChange = (event) => {
    const value = event.target.value;
    setCustomerAddress(value);
  };

  const handlePhoneNumberChange = (event) => {
    const value = event.target.value;
    setCustomerPhoneNumber(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.customer_name = customer_name;
    data.address = address;
    data.phone_number = phone_number;
    for (let item of customerData) {
      if (data.customer_name === item.customer_name) {
        const confirmCustomer = window.confirm("That customer already exists! Proceed?")
        if (!confirmCustomer) {
          setCustomerNameData("");
          setCustomerAddress("");
          setCustomerPhoneNumber("");
          event.target.reset();
          return
        }
      }
    }
    const customerUrl = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resp = await fetch(customerUrl, fetchConfig);
    if (resp.ok) {
      setCustomerNameData("");
      setCustomerAddress("");
      setCustomerPhoneNumber("");
      event.target.reset();
      alert("Created new customer!");
    } else {
      alert("Failed to create new customer");
    }
  };

  return (
<div className="table-container">
  <div className = "link-button-div-form">
    <button
      className="btn btn-primary"
      id="link-button-spacing"
    >
      <Link
      to="/customers"
      id="link-button">
        View Customers
      </Link>
    </button>
  </div>
  <div className="my-5">
    <div className="offset-3 col-10" id="form-style">
      <div className="shadow p-4 mt-4">
        <h1>Add a New Customer</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
              onChange={handleNameChange}
              placeholder="customer_name"
              required
              type="text"
              name="customer_name"
              id="customer_name"
              className="form-control"
            />
            <label htmlFor="customer_name">Customer Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleAddressChange}
              placeholder="address"
              required
              type="text"
              name="address"
              id="address"
              className="form-control"
            />
            <label htmlFor="address">Customer Address</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handlePhoneNumberChange}
              placeholder="phone_number"
              required
              type="text"
              name="phone_number"
              id="phone_numberr"
              className="form-control"
            />
            <label htmlFor="phone_number">Customer Phone Number</label>
          </div>
          <button className="btn btn-primary">Create Customer</button>
        </form>
      </div>
    </div>
  </div>
</div>
  );
};

export default CustomerForm;
