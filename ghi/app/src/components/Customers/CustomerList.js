import { useEffect, useState } from "react";
import { Link } from "react-router-dom"
import "../../index.css"

const CustomerList = () => {
  const [customerData, setCustomerData] = useState([]);
  const [filterTerm, setFilterTerm] = useState("");

  const getCustomerData = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      const sortedCustomers = data.customers.sort((a, b) =>
      a.customer_name.localeCompare(b.customer_name))
      setCustomerData(sortedCustomers);
    }
  };

  useEffect(() => {
    getCustomerData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  const handleDelete = async (e) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this Customer? This cannot be undone!");
    if (!confirmDelete) {
      return;
    }
    const customerUrl = `http://localhost:8090/api/customers/${e.target.id}/`;
    const fetchConfig = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      getCustomerData();
    } else {
      alert("Customer was not found!");
    }
  };

  return (
    <div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "7px", paddingRight: "7px"}}
        >
          <Link
          to="/customers/new"
          id="link-button">
            Create a Customer
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "17px", paddingRight: "17px"}}
        >
          <Link
          to="/salespersons"
          id="link-button">
            View Sales Staff
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/salesrecords"
          id="link-button">
            View Sales Records
          </Link>
        </button>
      </div>
      <table className="table table-striped">
        <thead>
        <div className="table-name">Customers</div>
          <div className="list-link-container">
            <div className="form-outline">
              <input
                onChange={handleFilterChange}
                type="search"
                id="form1"
                className="form-control"
                placeholder="To search for a specific Customer, enter name here"
                aria-label="Search"
                />
            </div>

          </div>
          <tr>
            <th>Customer Name</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {customerData &&
            customerData.filter((cust) =>
            cust.customer_name.toLowerCase().includes(filterTerm)
            )
            .map((cust) => {
              return (
                <tr key={cust.id}>
                  <td>{cust.customer_name}</td>
                  <td>{cust.address}</td>
                  <td>{cust.phone_number}</td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={handleDelete}
                      id={cust.id}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
};

export default CustomerList;
