import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom"

const ServiceAppointmentList = () => {
  const [appointments, setAppointment] = useState([]);
  const [filterTerm, setFilterTerm] = useState("");

  // const getAppointmentData = async () => {
  //   const resp = await fetch("http://localhost:8080/api/appointments/");
  //   if (resp.ok) {
  //     const data = await resp.json();

  //     const appointmentData = data.appointments.map((appt) => {
  //       return {
  //         id: appt.id,
  //         vin: appt.vin,
  //         ownersName: appt.owners_name,
  //         date: new Date(appt.date).toLocaleDateString(),
  //         time: appt.time,
  //         technicianName: appt.assigned_technician.name,
  //         reasonForService: appt.reason_for_service,
  //         isCompleted: appt.is_completed,
  //         isVip: appt.is_vip,
  //       };
  //     });
  //     setAppointment(appointmentData);
  //   }
  // };

  const getAppointmentData = async () => {
    const resp = await fetch("http://localhost:8080/api/appointments/");
    if (resp.ok) {
      const data = await resp.json();
      console.log(data.appointments)
      const sortedAppointments = data.appointments.sort((a, b) =>
      new Date(a.date).getTime() - new Date(b.date).getTime()
      );
      setAppointment(sortedAppointments);
    }
  };

  useEffect(() => {
    getAppointmentData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  const handleDelete = async (e) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this Appointment? This cannot be undone!");
    if (!confirmDelete) {
      return;
    }
    const url = `http://localhost:8080/api/appointments/${e.target.id}/`;
    const fetchConfigs = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const resp = await fetch(url, fetchConfigs);
    const data = await resp.json()
    setAppointment(
      appointments.filter((appt) => String(appt.vin) !== e.target.id)
    );
  };

  const handleComplete = async (e) => {
    const url = `http://localhost:8080/api/appointments/${e.target.id}/`;

    const fetchConfigs = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ is_completed: "✓" }),
    };
    const response = await fetch(url, fetchConfigs);
    const data = await response.json();

    setAppointment(
      appointments.filter((appt) => String(appt.vin) !== e.target.id)
    );
  };

  return (
    <div className="table-container">
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/appointments/new"
          id="link-button">
            Schedule New Appointment
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/appointments/search/"
          id="link-button">
            Search Appointment History
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "46px", paddingRight: "46px"}}
        >
          <Link
          to="/technicians"
          id="link-button">
            View Technicians
          </Link>
        </button>
      </div>
      <table className="table table-striped">
        <thead >
          <div className="table-name">Appointments</div>
          <div className="form-outline">
            <input
            onChange={handleFilterChange}
            type="search"
            id="form1"
            className="form-control"
            placeholder="Search by Owner's Name"
            aria-label="Search"
            />
          </div>
          <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason for Service</th>
            <th>Vip?</th>
          </tr>
        </thead>
        <tbody>
          {appointments
            .filter((appt) => appt.is_completed === "✘")
            .filter((appt) => appt.owners_name.toLowerCase().includes(filterTerm))
            .map((appt) => {
              return (
                <tr key={appt.id}>
                  <td>{appt.vin}</td>
                  <td>{appt.owners_name}</td>
                  <td>{new Date(appt.date).toLocaleDateString()}</td>
                  <td>{appt.time}</td>
                  <td>{appt.assigned_technician.name}</td>
                  <td>{appt.reason_for_service}</td>
                  <td>{appt.is_vip}</td>
                  <td>
                    <button
                      onClick={handleDelete}
                      id={appt.vin}
                      className="btn btn-danger"
                    >
                      Cancel
                    </button>
                  </td>
                  <td>
                    <button
                      onClick={handleComplete}
                      id={appt.vin}
                      className="btn btn-primary"
                    >
                      Complete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
};

export default ServiceAppointmentList;
