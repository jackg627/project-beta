import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom"
import '../../index.css'

const ServiceAppointmentForm = () => {
  const [vin, setVin] = useState("");
  const [owners_name, setOwnersName] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [reason_for_service, setReasonForService] = useState("");
  const [assigned_technician_id, setTechnician] = useState("");
  const [assigned_technician, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);

  const getAppointments = async () => {
    const appointmentsUrl = "http://localhost:8080/api/appointments/";
    const response = await fetch(appointmentsUrl);
    if (response.ok) {
      const data = await response.json()
      setAppointments(data.appointments)
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const [hours, minutes] = time.split(":")
    const formattedTime = `${hours.padStart(2, "0")}:${minutes.padStart(2, "0")}`;
    const formattedDate = new Date(date).toISOString().split("T")[0];

    const data = {};
    data.vin = vin;
    data.owners_name = owners_name;
    data.date = formattedDate;
    data.time = formattedTime;
    data.reason_for_service = reason_for_service;
    data.assigned_technician_id = assigned_technician_id;
    data.assigned_technician = assigned_technician;

    for (let item of appointments) {
      if (data.vin === item.vin && item.is_completed === "✘") {
        const confirmAppt = window.confirm("An appointment already exists for this vehicle! Proceed?")
        if (!confirmAppt) {
          setVin("");
          setOwnersName("");
          setDate("");
          setTime("");
          setReasonForService("");
          setTechnician("");
          return
        }
      }
    }

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfigs = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resp = await fetch(appointmentUrl, fetchConfigs);
    if (resp.ok) {
      setVin("");
      setOwnersName("");
      setDate("");
      setTime("");
      setReasonForService("");
      setTechnician("");
      alert("Successfully created a new appointment!");
    } else {
      alert("Failed to create a new appointment");
    }
  };

  const getTechnicianData = async () => {
    const resp = await fetch("http://localhost:8080/api/technicians/");
    if (resp.ok) {
      const data = await resp.json();
      const sortedTechnicians = data.technicians.sort((a, b) =>
      a.name.localeCompare(b.name)
      );
      setTechnicians(sortedTechnicians);
    }
  };

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleOwnersNameChange = (event) => {
    const value = event.target.value;
    setOwnersName(value);
  };

  const handleDateChange = (event) => {
    const value = event.target.value;
    setDate(value);
  };

  const handleTimeChange = (event) => {
    const value = event.target.value;
    setTime(value);
  };

  const handleReasonForServiceChange = (event) => {
    const value = event.target.value;
    setReasonForService(value);
  };

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  useEffect(() => {
    getTechnicianData();
    getAppointments();
  }, []);

  return (
    <div className="table-container">
      <div className = "link-button-div-form">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/appointments"
          id="link-button">
            View Appointments
          </Link>
        </button>
      </div>
      <div className="my-5">
        <div className="offset-3 col-6" id="form-style">
          <div className="shadow p-4 mt-4">
            <h1>Add a new appointment</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input
                  value={vin}
                  onChange={handleVinChange}
                  placeholder="vin"
                  required
                  type="text"
                  id="vin"
                  className="form-control"
                  name="vin"
                />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={owners_name}
                  onChange={handleOwnersNameChange}
                  placeholder="owners_name"
                  required
                  type="text"
                  id="owners_name"
                  className="form-control"
                  name="owners_name"
                />
                <label htmlFor="owners_name">Owner's Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={date}
                  onChange={handleDateChange}
                  placeholder="date"
                  required
                  type="date"
                  id="date"
                  className="form-control"
                  name="date"
                />
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={time}
                  onChange={handleTimeChange}
                  placeholder="time"
                  required
                  type="time"
                  id="time"
                  className="form-control"
                  name="time"
                />
                <label htmlFor="time">Time</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={reason_for_service}
                  onChange={handleReasonForServiceChange}
                  placeholder="reason_for_service"
                  required
                  type="text"
                  id="reason_for_service"
                  className="form-control"
                  name="reason_for_service"
                />
                <label htmlFor="reason_for_service">
                  Reason for service
                </label>
              </div>
              <div className="form-floating mb-3">
                <select
                  value={assigned_technician_id}
                  onChange={handleTechnicianChange}
                  required
                  id="assigned_technician"
                  className="form-control"
                  name="assigned_technician"
                >
                  <option value="">Choose a technician</option>
                  {assigned_technician.map((tech) => {
                    return (
                      <option key={tech.href} value={tech.id}>
                        {tech.name}
                      </option>
                    );
                  })}
                  ;
                </select>
              </div>
              <button className="btn btn-primary">Create Appointment</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ServiceAppointmentForm;
