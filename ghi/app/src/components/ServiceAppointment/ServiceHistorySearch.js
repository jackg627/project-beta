import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom"

const ServiceHistorySearch = () => {
  const [appointments, setAppointment] = useState([]);
  const [filterTerm, setFilterTerm] = useState("");

  const getAppointmentData = async () => {
    const resp = await fetch("http://localhost:8080/api/appointments/");
    if (resp.ok) {
      const data = await resp.json();

      const appointmentData = data.appointments.map((appt) => {
        return {
          id: appt.id,
          vin: appt.vin,
          ownersName: appt.owners_name,
          date: new Date(appt.date).toLocaleDateString(),
          time: appt.time,
          technicianName: appt.assigned_technician.name,
          reasonForService: appt.reason_for_service,
          isCompleted: appt.is_completed,
          isVip: appt.is_vip,
        };
      });

      setAppointment(appointmentData);
    }
  };

  useEffect(() => {
    getAppointmentData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  return (
    <div className="table-container">
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/appointments"
          id="link-button">
            Back to Appointments
          </Link>
        </button>
      </div>
      <table className="table table-striped">
        <thead>
        <div className="table-name">Service History</div>
        <div className="form-outline">
        <input
        onChange={handleFilterChange}
        placeholder="Type a VIN"
        type="search"
        id="form1"
        className="form-control"
        aria-label="Search"
        />
      </div>
          <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason for Service</th>
            <th>Completed?</th>
            <th>Vip?</th>
          </tr>
        </thead>

        <tbody>
          {appointments
            .filter((appt) => appt.vin.toLowerCase().includes(filterTerm))
            .filter((appt) => appt.isCompleted === "✓")
            .map((appt) => {
              return (
                <tr key={appt.id}>
                  <td>{appt.vin}</td>
                  <td>{appt.ownersName}</td>
                  <td>{appt.date}</td>
                  <td>{appt.time}</td>
                  <td>{appt.technicianName}</td>
                  <td>{appt.reasonForService}</td>
                  <td>{appt.isCompleted}</td>
                  <td>{appt.isVip}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
};

export default ServiceHistorySearch;
