import { useState, useEffect } from "react";
import { Link } from "react-router-dom"

const TechnicianList = () => {
  const [technicians, setTechnician] = useState([]);
  const [filterTerm, setFilterTerm] = useState("");

  const getTechnicianData = async () => {
    const resp = await fetch("http://localhost:8080/api/technicians/");
    if (resp.ok) {
      const data = await resp.json();
      const sortedTechnicians = data.technicians.sort((a, b) =>
      a.name.localeCompare(b.name)
      );
      setTechnician(sortedTechnicians);
    }
  };

  useEffect(() => {
    getTechnicianData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  const handleDelete = async (e) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this Technician? This cannot be undone!");
    if (!confirmDelete) {
      return;
    }
    const url = `http://localhost:8080/api/technicians/${e.target.id}`;

    const fetchConfig = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const resp = await fetch(url, fetchConfig);
    const data = await resp.json();
    setTechnician(
      technicians.filter(
        (technician) => String(technician.id) !== e.target.id
      )
    );
  };

  return (
    <div className="table-container">
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/technicians/new"
          id="link-button">
            Add New Technician
          </Link>
        </button>
      </div>
      <div className = "link-button-div-list">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
          style={{paddingLeft: "9px"}}
        >
          <Link
          to="/appointments"
          id="link-button">
            View Appointments
          </Link>
        </button>
      </div>
      <table className="table table-striped">
        <thead>
        <div className="table-name">Technician Staff</div>
        <div className="form-outline">
        <input
          onChange={handleFilterChange}
          type="search"
          id="form1"
          className="form-control"
          placeholder="To search for a specific Salesperson, enter name here"
          aria-label="Search"
        />
      </div>
          <tr>
            <th>Name</th>
            <th>Technician ID</th>
          </tr>
        </thead>

        <tbody>
          {technicians.filter((technician) =>
          technician.name.toLowerCase().includes(filterTerm)
          )
          .map((technician) => {
            return (
              <tr key={technician.id}>
                <td>{technician.name}</td>
                <td>{technician.id}</td>
                {/* <td><Link to={`/technicians/${technician.id}`}>{ technician.name }</Link></td> */}
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={handleDelete}
                    id={technician.id}

                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default TechnicianList;
