import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom"

const TechnicianForm = () => {
  const [name, setName] = useState("");
  const [employee_number, setEmployeeNumber] = useState("");
  const [technicians, setTechnicians] = useState([])

  const getTechnicians = async () => {
    const techniciansUrl = "http://localhost:8080/api/technicians/";
    const response = await fetch(techniciansUrl)
    if (response.ok) {
      const data = await response.json()
      setTechnicians(data.technicians)
    }
  }

  useEffect(() => {
    getTechnicians();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.employee_number = employee_number;

    for (let item of technicians) {
      if (data.name === item.name) {
        const confirmTechnician = window.confirm("A technician with this name already exists! Proceed?")
        if (!confirmTechnician) {
          setName("");
          setEmployeeNumber("");
          return;
        }
      }
    }

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resp = await fetch(technicianUrl, fetchConfig);
    if (resp.ok) {
      setName("");
      setEmployeeNumber("");
      alert("Created new technician!")
    } else {
      alert("Failed to create new technician")
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleEmployeeNumberChange = (event) => {
    const value = event.target.value;
    setEmployeeNumber(value);
  };

  return (
    <div className="table-container">
      <div className = "link-button-div-form">
        <button
          className="btn btn-primary"
          id="link-button-spacing"
        >
          <Link
          to="/technicians"
          id="link-button">
            View Technicians
          </Link>
        </button>
      </div>
      <div className="my-5">
        <div className="offset-3 col-6" id="form-style">
          <div className="shadow p-4 mt-4">
            <h1>Add a new technician</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  value={name}
                  onChange={handleNameChange}
                  placeholder="Name"
                  required
                  type="text"
                  id="name"
                  className="form-control"
                  name="name"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={employee_number}
                  onChange={handleEmployeeNumberChange}
                  placeholder="employee_number"
                  required
                  type="text"
                  id="employee_number"
                  className="form-control"
                  name="employee_number"
                />
                <label htmlFor="employee_number">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create Technician</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TechnicianForm;
