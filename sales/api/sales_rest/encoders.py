from django.http import JsonResponse  # necessary? Check prior projects
from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, SalesRecord


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "salesperson_name",
        "employee_number",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "customer_name",
        "address",
        "phone_number",
        "id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
    ]


class SalesrecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "customer": CustomerEncoder(),
        "salesperson": SalespersonEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "vin": o.automobile.vin,
        }
