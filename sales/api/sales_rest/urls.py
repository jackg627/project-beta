from django.urls import path

from .views import (
    api_salespeople_list,
    api_customers_list,
    api_salesrecord_list,
    api_show_customer,
    api_show_salesperson,
)

urlpatterns = [
    path("salespeople/", api_salespeople_list, name="api_salespeople_list"),
    path(
        "salespeople/<int:id>/", api_show_salesperson,
        name="api_show_salesperson"
    ),
    path("customers/", api_customers_list, name="api_customers_list"),
    path(
        "customers/<int:id>/", api_show_customer, name="api_show_customer"
    ),
    path("salesrecords/", api_salesrecord_list, name="api_salesrecord_list"),

]
