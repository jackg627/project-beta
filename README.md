# CarCar

Team:

- Jakob Schweter - Service Microservice
- Jack Glenn - Sales Microservice

## Design

The CarCar application incorporates three separate microservices: Inventory, Service, and Sales. These are interacted with via the browser using React as a frontend service, and Django/Docker for backend services.

## Service microservice

Models:
ServiceAppointment:
Has all parameters that are outlined in the instructions as well as two more "is_vip" and "is_completed" that can be changed to show if the specific appointment should be a VIP or not. Is completed becomes a check when the finish button is clicked in the appointments list. ServiceAppointment also compares its VIN with the polled vin from AutomobileVO to determine if the vin in ServiceAppointment should be a VIP. If any of the vins in the AutomobileVO match the vin the for the current appointment, is_vip will be set to a check.

Technician:
The technician model is a simple model that that holds technicians with "name" and "employee_number" values. ServiceAppointment has a foreign key to Technician that allows ServiceAppointment to assign a specifc technican to a specific appointment.

My bounded context was the Service microservice as a whole. It uses the AutomobileVO to take data from the inventory microservice and implement that data in the Service microservice.

## Sales microservice

The Sales Microservice is it's own Bounded Context. It owns all of its data, and polls the Inventory Microservice/Bounded Context for inventory information that Inventory chooses to share with Sales. In this way Sales can access information it needs from Inventory without having to gain access to all of Inventory's owned data.

The Sales Microservice incorporates four Models: AutomobileVO, Salesperson, Customer, and SalesRecord. AutomobileVO uses a poller to regularly inspect the status of the Inventory Microservice's Automobile Model in order to keep track of what cars are currently in inventory. The Customer and Salesperson models are fairly basic, each used to create and list their own respective instances with descriptive properties. Finally, the SalesRecord model incorporates foreign keys to each of the other three Sales Microservice models. Using this data plus a price attribute, each SalesRecord instance is able to record the specific automobile bought from inventory, the purchasing customer, the selling salesperson, and the sale price.

In the React app, at ghi/app/src/SalesRecordForm, the automobile dropdown tab compares "vin" data from the AutomobileVO and the database list of sales records. It displays as sale options all "vin"s that are found in AutomobileVO (inventory), but do not appear in the database list of Sales Records (already sold vehicles).
