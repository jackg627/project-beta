import json
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Technician, ServiceAppointment, AutomobileVO
from django.http import JsonResponse
from .encoders import TechnicianEncoder, ServiceAppointmentEncoder


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians}, encoder=TechnicianEncoder
        )

    else:
        content = json.loads(request.body)

        technicians = Technician.objects.create(**content)
        return JsonResponse(technicians, encoder=TechnicianEncoder, safe=False)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            {"technician": technician}, encoder=TechnicianEncoder
        )

    elif request.method == "DELETE":
        try:
            count, _ = Technician.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist!"})

    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=id).update(**content)
        technician = Technician.objects.get(id=id)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=ServiceAppointmentEncoder
        )

    else:
        content = json.loads(request.body)
        technician_id = content["assigned_technician_id"]
        assigned_technician = Technician.objects.get(id=technician_id)
        content["assigned_technician"] = assigned_technician

        auto_vo = AutomobileVO.objects.all().values()
        for key in auto_vo:
            if content["vin"] in key["import_vin"]:
                content["is_vip"] = "✓"

        appointments = ServiceAppointment.objects.create(**content)

        return JsonResponse(
            appointments, encoder=ServiceAppointmentEncoder, safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request, vin):
    if request.method == "GET":
        appointment = ServiceAppointment.objects.get(vin=vin)
        return JsonResponse(
            appointment, encoder=ServiceAppointmentEncoder, safe=False
        )
    elif request.method == "DELETE":
        try:
            count, _ = ServiceAppointment.objects.filter(vin=vin).delete()
            return JsonResponse({"deleted": count > 0})

        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})

    else:
        content = json.loads(request.body)
        ServiceAppointment.objects.filter(vin=vin).update(**content)
        appointment = ServiceAppointment.objects.get(vin=vin)
        return JsonResponse(
            appointment, encoder=ServiceAppointmentEncoder, safe=False
        )
