from common.json import ModelEncoder
from .models import Technician, ServiceAppointment, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = (AutomobileVO,)
    properties = ["import_href", "import_vin"]


class TechnicianEncoder(ModelEncoder):
    model = (Technician,)
    properties = ["name", "employee_number", "id"]


class ServiceAppointmentEncoder(ModelEncoder):
    model = (ServiceAppointment,)
    properties = [
        "id",
        "vin",
        "owners_name",
        "date",
        "time",
        "reason_for_service",
        "assigned_technician",
        "is_vip",
        "is_completed",
    ]
    encoders = {
        "assigned_technician": TechnicianEncoder(),
    }
