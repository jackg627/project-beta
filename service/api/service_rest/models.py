from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    import_vin = models.CharField(max_length=17)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"id": self.id})


class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=17)
    owners_name = models.CharField(max_length=100)
    date = models.DateTimeField()
    time = models.CharField(max_length=10)
    reason_for_service = models.CharField(max_length=100)
    assigned_technician = models.ForeignKey(
        Technician,
        related_name="serviceappointments",
        on_delete=models.CASCADE,
    )
    is_vip = models.CharField(max_length=10, default="✘")
    is_completed = models.CharField(max_length=10, default="✘")
