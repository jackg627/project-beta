# Generated by Django 4.0.3 on 2023-03-07 23:06

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("service_rest", "0005_serviceappointment_is_vip"),
    ]

    operations = [
        migrations.AlterField(
            model_name="serviceappointment",
            name="is_vip",
            field=models.BooleanField(default=False),
        ),
    ]
